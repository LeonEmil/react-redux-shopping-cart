import store from './store'
import { bugAdd, bugRemove } from './actionCreators'

const unsubscribe = store.subscribe(() => {
    console.log("State changed!", store.getState())
})

store.dispatch(bugAdd("Bug 1"))

unsubscribe()

store.dispatch(bugRemove(1))

console.log(state.getState())