import * as actions from "./actionTypes"

let lastId = 0

function reducer(state = [], action){
    if(action.type === actions.BUG_ADD){
        return [
            ...state,
            {
                id: ++lastId,
                description: action.description,
                resolved: false
            }
        ]
    }

    else if(action.type === actions.BUG_REMOVE){
        return state.filter(bug => bug.id !== action.id)
    }

    return state
}

export default reducer