import * as actions from './actionTypes'

const bugAdd = (description) => {
    return {
        type: actions.BUG_ADD,
        description: description
    }
}

const bugRemove = id => {
    return {
        type: actions.BUG_REMOVE,
        id: id
    }
}

export { bugAdd, bugRemove }